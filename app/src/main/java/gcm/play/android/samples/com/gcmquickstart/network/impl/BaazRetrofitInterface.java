package gcm.play.android.samples.com.gcmquickstart.network.impl;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * @author lukasz.czarnecki
 */
public interface BaazRetrofitInterface {

    @POST("/users/{user}/devices")
    void addDevice(@Path("user") final String userId, @Body AddDevicePayload payload, final Callback<Object> callback);
}
