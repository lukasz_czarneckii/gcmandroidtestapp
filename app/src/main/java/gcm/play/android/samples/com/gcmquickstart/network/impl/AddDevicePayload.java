package gcm.play.android.samples.com.gcmquickstart.network.impl;

import com.google.gson.annotations.SerializedName;

/**
 * @author lukasz.czarnecki
 */
public final class AddDevicePayload {

    @SerializedName("type")
    private final String mDeviceType;

    @SerializedName("device_id")
    private final String mDeviceId;

    @SerializedName("token")
    private final String mDeviceToken;

    public AddDevicePayload(final String deviceId, final String deviceToken) {
        mDeviceType = "Android";
        mDeviceId = deviceId;
        mDeviceToken = deviceToken;
    }
}
