package gcm.play.android.samples.com.gcmquickstart.network;

import gcm.play.android.samples.com.gcmquickstart.persistence.ResultCallback;

/**
 * @author lukasz.czarnecki
 */
public interface BaazNetworkInterface {

    void addDevice(final String userId, final String deviceId, final String deviceToken, final ResultCallback<Object> callback);
}
