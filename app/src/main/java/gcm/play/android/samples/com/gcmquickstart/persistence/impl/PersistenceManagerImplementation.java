package gcm.play.android.samples.com.gcmquickstart.persistence.impl;

import gcm.play.android.samples.com.gcmquickstart.R;
import gcm.play.android.samples.com.gcmquickstart.event.Event;
import gcm.play.android.samples.com.gcmquickstart.persistence.PersistenceManager;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import java.util.UUID;

import de.greenrobot.event.EventBus;

/**
 * @author lukasz.czarnecki
 */
public class PersistenceManagerImplementation implements PersistenceManager {

    private final SharedPreferences mPreferences;

    private String mApiEndpoint;

    private String mSenderId;

    private String mUserId;

    private String mDeviceId;

    private String mDeviceToken;

    public PersistenceManagerImplementation(final Context context) {
        mPreferences = context.getSharedPreferences(PersistenceManagerImplementation.class.getCanonicalName(), Context.MODE_PRIVATE);
        generateDeviceId();
        mApiEndpoint = mPreferences.getString(PersistanceKey.API_ENDPOINT, context.getString(R.string.default_api_endpoint));
        mSenderId = mPreferences.getString(PersistanceKey.SENDER_ID, context.getString(R.string.default_sender_id));
        mUserId = mPreferences.getString(PersistanceKey.USER_ID, context.getString(R.string.default_user_id));
        mDeviceId = mPreferences.getString(PersistanceKey.DEVICE_ID, null);
        mDeviceToken = mPreferences.getString(PersistanceKey.DEVICE_TOKEN, null);
    }

    @Override
    public String getApiEndpoint() {
        return mApiEndpoint;
    }

    @Override
    public String getSenderId() {
        return mSenderId;
    }

    @Override
    public String getUserId() {
        return mUserId;
    }

    @Override
    public String getDeviceId() {
        return mDeviceId;
    }

    @Override
    public String getDeviceToken() {
        return mDeviceToken;
    }

    public void setApiEndpoint(final String apiEndpoint) {
        mApiEndpoint = apiEndpoint;
    }

    public void setSenderId(final String senderId) {
        mSenderId = senderId;
    }

    public void setUserId(final String userId) {
        mUserId = userId;
    }

    public void setDeviceId(final String deviceId) {
        mDeviceId = deviceId;
    }

    public void setDeviceToken(final String deviceToken) {
        mDeviceToken = deviceToken;
    }

    @Override
    public void onActivityCreated(final Activity activity, final Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
    }

    @Override
    public void onActivityStarted(final Activity activity) {

    }

    @Override
    public void onActivityResumed(final Activity activity) {

    }

    @Override
    public void onActivityPaused(final Activity activity) {

    }

    @Override
    public void onActivityStopped(final Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(final Activity activity, final Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(final Activity activity) {
        EventBus.getDefault().unregister(this);
    }

    public void onEvent(final Event.OnGcmTokenReceived event) {
        mPreferences.edit().putString(PersistanceKey.DEVICE_TOKEN, event.getToken()).apply();
        mDeviceToken = event.getToken();
        EventBus.getDefault().post(new Event.OnRefresh());
    }

    private void generateDeviceId() {
        if (mPreferences.getString(PersistanceKey.DEVICE_ID, null) != null) {
            return;
        }
        final String deviceId = UUID.randomUUID().toString();
        mPreferences.edit().putString(PersistanceKey.DEVICE_ID, deviceId).apply();
    }

    private interface PersistanceKey {
        String API_ENDPOINT = "API_ENDPOINT";
        String SENDER_ID = "SENDER_ID";
        String USER_ID = "USER_ID";
        String DEVICE_ID = "DEVICE_ID";
        String DEVICE_TOKEN = "DEVICE_TOKEN";
    }
}
