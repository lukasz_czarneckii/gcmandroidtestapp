package gcm.play.android.samples.com.gcmquickstart.event;

/**
 * @author lukasz.czarnecki
 */
public final class Event {

    public static final class OnGcmTokenReceived {

        private final String mToken;

        public OnGcmTokenReceived(final String token) {
            mToken = token;
        }

        public String getToken() {
            return mToken;
        }
    }

    public static final class OnRefresh {

    }
}
