package gcm.play.android.samples.com.gcmquickstart.persistence;

import android.app.Application;

/**
 * @author lukasz.czarnecki
 */
public interface PersistenceManager extends Application.ActivityLifecycleCallbacks {

    String getApiEndpoint();

    void setApiEndpoint(final String apiEndpoint);

    String getSenderId();

    void setSenderId(final String senderId);

    String getUserId();

    void setUserId(final String userId);

    String getDeviceId();

    void setDeviceId(final String deviceId);

    String getDeviceToken();

    void setDeviceToken(final String deviceToken);
}
