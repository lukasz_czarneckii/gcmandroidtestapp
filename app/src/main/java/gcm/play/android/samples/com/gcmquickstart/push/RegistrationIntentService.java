/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gcm.play.android.samples.com.gcmquickstart.push;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import gcm.play.android.samples.com.gcmquickstart.event.Event;
import gcm.play.android.samples.com.gcmquickstart.persistence.PersistenceManager;
import gcm.play.android.samples.com.gcmquickstart.persistence.impl.PersistenceManagerImplementation;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import de.greenrobot.event.EventBus;

public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            final PersistenceManager persistenceManager = new PersistenceManagerImplementation(this);
            final InstanceID instanceID = InstanceID.getInstance(this);
            final String token = instanceID.getToken(persistenceManager.getSenderId(), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            EventBus.getDefault().post(new Event.OnGcmTokenReceived(token));
        } catch (Exception e) {
            Log.d(TAG, "Failed to complete token refresh", e);
        }
    }
}
