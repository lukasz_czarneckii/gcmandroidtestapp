package gcm.play.android.samples.com.gcmquickstart.network.impl;

import gcm.play.android.samples.com.gcmquickstart.network.BaazNetworkInterface;
import gcm.play.android.samples.com.gcmquickstart.persistence.ResultCallback;
import com.squareup.okhttp.OkHttpClient;

import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

/**
 * @author lukasz.czarnecki
 */
public class BaazNetworkImplementation implements BaazNetworkInterface {

    private final BaazRetrofitInterface mRetrofitInterface;

    public BaazNetworkImplementation(final String apiEndpoint) {
        final OkHttpClient okHttpClient = getUnsafeOkHttpClient();
        final RestAdapter.Builder builder = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(apiEndpoint);
        if (okHttpClient != null) {
            builder.setClient(new OkClient(okHttpClient));
        }
        mRetrofitInterface = builder.build().create(BaazRetrofitInterface.class);
    }

    @Override
    public void addDevice(final String userId, final String deviceId, final String deviceToken, final ResultCallback<Object> callback) {
        String errorMessage = null;
        if (userId == null) {
            errorMessage = "User ID cannot be empty.";
        } else if (deviceId == null) {
            errorMessage = "Device ID cannot be empty.";
        } else if (deviceToken == null) {
            errorMessage = "Device token cannot be empty.";
        }

        if (errorMessage != null) {
            callback.error(errorMessage);
            return;
        }

        mRetrofitInterface.addDevice(userId, new AddDevicePayload(deviceId, deviceToken), new Callback<Object>() {

            @Override
            public void success(final Object o, final Response response) {
                callback.success(o);
            }

            @Override
            public void failure(final RetrofitError error) {
                if (error != null) {
                    callback.error("Server returned error: " + error.getMessage());
                } else {
                    callback.error("Server returned unknown error.");
                }
            }
        });
    }

    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient.setSslSocketFactory(sslSocketFactory);
            okHttpClient.setHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            return okHttpClient;
        } catch (Exception e) {
            //no-op
        }
        return null;
    }
}
