package gcm.play.android.samples.com.gcmquickstart;

import gcm.play.android.samples.com.gcmquickstart.event.Event;
import gcm.play.android.samples.com.gcmquickstart.network.BaazNetworkInterface;
import gcm.play.android.samples.com.gcmquickstart.network.impl.BaazNetworkImplementation;
import gcm.play.android.samples.com.gcmquickstart.persistence.PersistenceManager;
import gcm.play.android.samples.com.gcmquickstart.persistence.ResultCallback;
import gcm.play.android.samples.com.gcmquickstart.persistence.impl.PersistenceManagerImplementation;
import gcm.play.android.samples.com.gcmquickstart.push.RegistrationIntentService;
import gcm.play.android.samples.com.gcmquickstart.util.PlayServicesUtil;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.api_endpoint_value)
    EditText mApiEndpointEditText;

    @Bind(R.id.sender_id_value)
    EditText mSenderIdEditText;

    @Bind(R.id.user_id_value)
    EditText mUserIdEditText;

    @Bind(R.id.device_id_value)
    EditText mDeviceIdEditText;

    @Bind(R.id.device_token_value)
    TextView mDeviceTokenTextView;

    @Bind(R.id.button_send)
    Button mSendButton;

    private PersistenceManager mPersistenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        mPersistenceManager = new PersistenceManagerImplementation(this);
        mPersistenceManager.onActivityCreated(this, savedInstanceState);
        setupView();
        registerDeviceInGcm();
    }

    @Override
    protected void onDestroy() {
        mPersistenceManager.onActivityDestroyed(this);
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    public void onEventMainThread(final Event.OnRefresh event) {
        populateView();
    }

    private void registerDeviceInGcm() {
        if (PlayServicesUtil.checkPlayServices(this)) {
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    private void populateView() {
        mApiEndpointEditText.setText(mPersistenceManager.getApiEndpoint());
        mSenderIdEditText.setText(mPersistenceManager.getSenderId());
        mUserIdEditText.setText(mPersistenceManager.getUserId());
        mDeviceIdEditText.setText(mPersistenceManager.getDeviceId());
        mDeviceTokenTextView.setText(mPersistenceManager.getDeviceToken());
    }

    private void setupView() {
        mSendButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View v) {
                sendDataToServer(
                        mApiEndpointEditText.getText(),
                        mUserIdEditText.getText(),
                        mDeviceIdEditText.getText(),
                        mDeviceTokenTextView.getText());
            }
        });
        populateView();
    }

    private void sendDataToServer(final Editable apiEndpoint, final Editable userId, final Editable deviceId, final CharSequence deviceToken) {
        String errorMessage = null;
        if (apiEndpoint == null || apiEndpoint.length() == 0) {
            errorMessage = "Api endpoint cannot be empty.";
        } else if (userId == null || userId.length() == 0) {
            errorMessage = "User ID cannot be empty.";
        } else if (deviceId == null || deviceId.length() == 0) {
            errorMessage = "Device ID cannot be empty.";
        } else if (deviceToken == null || deviceToken.length() == 0) {
            errorMessage = "Device token cannot be empty.";
        }

        if (errorMessage != null) {
            Toast.makeText(MainActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
            return;
        }

        final BaazNetworkInterface networkInterface = new BaazNetworkImplementation(apiEndpoint.toString());
        networkInterface.addDevice(userId.toString(), deviceId.toString(), deviceToken.toString(), new ResultCallback<Object>() {

            @Override
            public void success(final Object result) {
                mPersistenceManager.setApiEndpoint(apiEndpoint.toString());
                mPersistenceManager.setUserId(userId.toString());
                mPersistenceManager.setDeviceId(deviceId.toString());
                mPersistenceManager.setDeviceToken(deviceToken.toString());
                Toast.makeText(MainActivity.this, "Data sent successfully.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void error(final String message) {
                Toast.makeText(MainActivity.this, "Error: " + message, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
