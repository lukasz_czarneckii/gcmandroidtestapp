package gcm.play.android.samples.com.gcmquickstart.persistence;

/**
 * @author lukasz.czarnecki
 */
public interface ResultCallback<T> {

    void success(final T result);

    void error(final String errorMessage);
}
